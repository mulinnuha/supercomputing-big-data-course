import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.Partitioner._
import org.apache.spark.RangePartitioner
import org.apache.spark.HashPartitioner
import org.apache.log4j.Logger
import org.apache.log4j.Level

import java.io._
import java.util.zip.GZIPOutputStream
import java.nio.file.{Paths, Files}
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.text.DecimalFormat
import java.util.Calendar

import org.apache.hadoop.mapreduce.lib.input.TextInputFormat
import org.apache.hadoop.io.LongWritable
import org.apache.hadoop.io.Text
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.io.compress.GzipCodec

import dataalgorithms.FastqInputFormat

object FastqChunker 
{

def getListOfFiles(dir: String):List[File] = {
	val d = new File(dir)
	if (d.exists && d.isDirectory) {
		d.listFiles.filter(_.isFile).toList
	} else {
		List[File]()
	}
}

// function to interleave the two fastq files
// by reading each line from the respective fastq file and combine them
def interleave(aiter: Iterator[(LongWritable, Text)], biter: Iterator[(LongWritable, Text)]): Iterator[(Int, String)] =
{
	var res = List[(Int, String)]()
	var i = 0
	while (aiter.hasNext && biter.hasNext)
	{	
		i = i+1
		val x = (i, aiter.next._2.toString + biter.next._2.toString)
		res ::= x
	}
	res.iterator
}

def main(args: Array[String]) 
{
	if (args.size < 3)
	{
		println("Not enough arguments!\nArg1 = number of parallel tasks = number of chunks\nArg2 = input folder\nArg3 = output folder")
		System.exit(1)
	}
	
	val prllTasks = args(0)
	val inputFolder = args(1)
	val outputFolder = args(2)
	
	if (!Files.exists(Paths.get(inputFolder)))
	{
		println("Input folder " + inputFolder + " doesn't exist!")
		System.exit(1)
	}
		 
	// Create output folder if it doesn't already exist
	new File(outputFolder).mkdirs
	
	println("Number of parallel tasks = number of chunks = " + prllTasks + "\nInput folder = " + inputFolder + "\nOutput folder = " + outputFolder)
	
	val conf = new SparkConf().setAppName("DNASeqAnalyzer")
	conf.setMaster("local[" + prllTasks + "]")
	conf.set("spark.cores.max", prllTasks)
	conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
	
	val sc = new SparkContext(conf)
	
	// Comment these two lines if you want to see more verbose messages from Spark
	Logger.getLogger("org").setLevel(Level.OFF);
	Logger.getLogger("akka").setLevel(Level.OFF);
		
	var t0 = System.currentTimeMillis
	
	// Rest of the code goes here
	val config 		= new Configuration()

	// The file name of the input file
	val firstFastq  = "file://" + inputFolder + "/fastq1.fq"
	val secondFastq = "file://" + inputFolder + "/fastq2.fq" 

	// HashPartitioner to partition the RDDs
	val hashPr 		= new HashPartitioner(prllTasks.toInt)

	// RDD1 from fastq1.fq
	// <LongWritable, short read>
	// Using custom record reader and input format from Data Algorithm book
	val rdd1		= sc.newAPIHadoopFile(firstFastq, classOf[FastqInputFormat], 
		classOf[LongWritable], classOf[Text], config).partitionBy(hashPr)

	// RDD2 from fastq2.fq
	val rdd2		= sc.newAPIHadoopFile(secondFastq, classOf[FastqInputFormat], 
		classOf[LongWritable], classOf[Text], config).partitionBy(hashPr)

	// Interleaving fast1.fq and fastq2.fq
	// Partition into the number of prllTasks
	val rdd3		= rdd1.zipPartitions(rdd2)(interleave).sortBy(_._1)
	
	// Write each partition to file
	val rdd4 = rdd3.mapPartitionsWithIndex{(index, iterator) => {
		val idx = index + 1
		val filename = outputFolder + "/chunk" + idx + ".fq.gz"
		val zip = new GZIPOutputStream(new FileOutputStream(new File(filename)));
		val bw = new BufferedWriter(new OutputStreamWriter(zip, "UTF-8"))
		val x = iterator.toList.map(_._2)
		x.foreach(bw.write)
		bw.close
		x.iterator
	}}.collect
	
	val et = (System.currentTimeMillis - t0) / 1000 
	println("|Execution time: %d mins %d secs|".format(et/60, et%60))
}
//////////////////////////////////////////////////////////////////////////////
} // End of Class definition
