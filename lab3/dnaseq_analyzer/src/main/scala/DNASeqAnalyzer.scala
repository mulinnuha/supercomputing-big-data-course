/* 
 * Copyright (c) 2015-2016 TU Delft, The Netherlands.
 * All rights reserved.
 * 
 * You can redistribute this file and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Authors: Hamid Mushtaq
 *
*/
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.HashPartitioner
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.commons.lang3.StringUtils

import sys.process._

import scala.collection.mutable.ArrayBuffer
import scala.util.Sorting
import scala.io.Source

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.text.DecimalFormat
import java.util.Calendar
import java.io._

import tudelft.utils.ChromosomeRange
import tudelft.utils.DictParser
import tudelft.utils.Configuration
import tudelft.utils.SAMRecordIterator

import htsjdk.samtools._

object DNASeqAnalyzer 
{
final val MemString = "-Xmx5120m" //Xmx6144m //
final val RefFileName = "ucsc.hg19.fasta"
final val SnpFileName = "dbsnp_138.hg19.vcf"
final val ExomeFileName = "gcat_set_025.bed"
final val NumOfChromosomes = 24
//////////////////////////////////////////////////////////////////////////////

def bwaRun(x: String, config: Configuration): Array[(Int, SAMRecord)] = 
{
	val refFolder   = config.getRefFolder
	val tmpFolder   = config.getTmpFolder
	val toolsFolder = config.getToolsFolder
	val inputFolder = config.getInputFolder
	val outputFolder= config.getOutputFolder
	val numOfThreads= config.getNumThreads
	
	// Create the tmp folder if it doesn't already exist
	new File(tmpFolder).mkdirs
	
	// Create the outputFolder/log/bwa to put the log
	val bwaLogFolder= outputFolder + "log/bwa"
	val bwaLogFile  = bwaLogFolder + "/" + StringUtils.substringBefore(x, ".fq.gz") + "_log.txt"
	new File(bwaLogFolder).mkdirs

	// Create the command string (bwa mem...)and then execute it using the Scala's process package. More help about 
	//	Scala's process package can be found at http://www.scala-lang.org/api/current/index.html#scala.sys.process.package. 

	val bwaBw       = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(bwaLogFile), "UTF-8"))

	// unzip file & write log
	val gzFile			= inputFolder + x
	val gzFileSize		= new File(gzFile).length
	bwaBw.write(getTimeStamp() + " gunzip -c " + inputFolder + x + ". Size of input file = " + gzFileSize + " bytes)." + "\n")
	val gunzipCommand	= "gunzip -c " + gzFile
	val tmpFileName		= tmpFolder + StringUtils.substringBefore(x, ".gz")
	val resultGunzip	= (gunzipCommand #> new File(tmpFileName)).!!

	// bwa mem refFolder/RefFileName -p -t numOfThreads fastqChunk > outFileName
	val bwaMemCommand 	= toolsFolder + "bwa mem " + refFolder + RefFileName + " -p -t " + numOfThreads + " " + tmpFileName
	bwaBw.write(getTimeStamp() + " " + bwaMemCommand + "\n")
	val outFileName   	= tmpFolder + StringUtils.substringBefore(x, ".gz") + ".sam"
	val resultBwaMem 	= (bwaMemCommand #> new File(outFileName)).!!
	val outFileLines	= Source.fromFile(outFileName).getLines.size
	bwaBw.write(getTimeStamp() + " bwa mem completed for " + StringUtils.substringBefore(x, ".gz") + ". Number of key value pairs = " +
		outFileLines + "." + "\n")

	val bwaKeyValues    = new BWAKeyValues(outFileName)
	bwaKeyValues.parseSam()
	val kvPairs: Array[(Int, SAMRecord)] = bwaKeyValues.getKeyValuePairs()
	
	// Delete the temporary files
	bwaBw.write(getTimeStamp() + " Delete the temporary files." + "\n")
	val rmCommand		= ("rm " + outFileName + " " + tmpFileName).!
	bwaBw.close
	// Return kvPairs
	return kvPairs
}
	 
def writeToBAM(fileName: String, samRecordsSorted: Array[SAMRecord], config: Configuration) : ChromosomeRange = 
{
	val header = new SAMFileHeader()
	header.setSequenceDictionary(config.getDict())
	val outHeader = header.clone()
	outHeader.setSortOrder(SAMFileHeader.SortOrder.coordinate);
	val factory = new SAMFileWriterFactory();
	val writer = factory.makeBAMWriter(outHeader, true, new File(fileName));
	
	val r = new ChromosomeRange()
	val input = new SAMRecordIterator(samRecordsSorted, header, r)
	while(input.hasNext()) 
	{
		val sam = input.next()
		writer.addAlignment(sam);
	}
	writer.close();
	
	return r
}

// variant call wrapper
def variantCallWrapper(iter: Iterator[(Int, SAMRecord)], config: Configuration): Iterator[(String, (Integer, String))] =
{
	var res = Array.empty[(String, (Integer, String))]
	val arrayBuf = ArrayBuffer.empty[SAMRecord]
	var key = 0

	while (iter.hasNext){
		val x = iter.next
		key = x._1
		arrayBuf.append(x._2)
	}
	
	if (!arrayBuf.isEmpty){
		implicit val samRecordOrdering = new Ordering[SAMRecord]{
			override def compare(a: SAMRecord, b: SAMRecord): Int = {
				val refIndex1 = a.getReferenceIndex()
				val refIndex2 = b.getReferenceIndex()
				var result = 0

				if (refIndex1 == -1) {
					if(refIndex2 == -1) result = 0 else result = 1
				} else if (refIndex2 == -1) {
					result = -1
				}

				val cmp = refIndex1 - refIndex2;
				if (cmp != 0) {
					result = cmp;
				} else {
					result = a.getAlignmentStart() - b.getAlignmentStart()
				}
				return result
			}
		}
		val samRecordsSorted: Array[SAMRecord] = arrayBuf.toArray
		scala.util.Sorting.quickSort(samRecordsSorted)

		res = variantCall(key, samRecordsSorted, config)
	}

	res.iterator
}

def variantCall(chrRegion: Int, samRecordsSorted: Array[SAMRecord], config: Configuration): Array[(String, (Integer, String))] = 
{
	val refFolder   = config.getRefFolder
	val tmpFolder   = config.getTmpFolder
	val toolsFolder = config.getToolsFolder
	val inputFolder = config.getInputFolder
	val outputFolder= config.getOutputFolder
	val numOfThreads= config.getNumThreads
	
	// Create the tmp folder if it doesn't already exist
	new File(tmpFolder).mkdirs
	
	// Following is shown how each tool is called. Replace the X in regionX with the chromosome region number (chrRegion). 
	// 	You would have to create the command strings (for running jar files) and then execute them using the Scala's process package. More 
	// 	help about Scala's process package can be found at http://www.scala-lang.org/api/current/index.html#scala.sys.process.package.
	//	Note that MemString here is -Xmx6144m, and already defined as a constant variable above, and so are reference files' names.
	
	// SAM records should be sorted by this point

	// Create the outputFolder/log/vc to put the log
	val vcLogFolder     = outputFolder + "log/vc"
	val vcLogFile       = vcLogFolder + "/" + chrRegion + "_log.txt"
	new File(vcLogFolder).mkdirs
	val vcBw           = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(vcLogFile, true), "UTF-8"))

	// Initialize file names
	val javaCommand     = "java " + MemString + " -jar " + toolsFolder
	val filenameP1      = tmpFolder + "region" + chrRegion + "-p1.bam"
	val filenameP2      = tmpFolder + "region" + chrRegion + "-p2.bam"
	val filenameP3      = tmpFolder + "region" + chrRegion + "-p3.bam"
	val filenameTmpBed  = tmpFolder + "tmp" + chrRegion + ".bed"
	val filenameBedBed  = tmpFolder + "bed" + chrRegion + ".bed"
	val filenameRegBam  = tmpFolder + "region" + chrRegion + ".bam"
	val filenameRegBam2 = tmpFolder + "region" + chrRegion + "-2.bam"
	val filenameRegBam3 = tmpFolder + "region" + chrRegion + "-3.bam"
	val filenameRegBai  = tmpFolder + "region" + chrRegion + ".bai"
	val filenameRegBai2 = tmpFolder + "region" + chrRegion + "-2.bai"
	val filenameRegBai3 = tmpFolder + "region" + chrRegion + "-3.bai"
	val filenameRegInt  = tmpFolder + "region" + chrRegion + ".intervals"
	val filenameRegTbl  = tmpFolder + "region" + chrRegion + ".table"
	val filenameVCF     = tmpFolder + "region" + chrRegion + ".vcf"
	val filenameMetrics = tmpFolder + "region" + chrRegion + "-p3-metrics.txt"


	val chrRange        = writeToBAM(filenameP1, samRecordsSorted, config)
	
	// Picard preprocessing
	//	java MemString -jar toolsFolder/CleanSam.jar INPUT=tmpFolder/regionX-p1.bam OUTPUT=tmpFolder/regionX-p2.bam
	//	java MemString -jar toolsFolder/MarkDuplicates.jar INPUT=tmpFolder/regionX-p2.bam OUTPUT=tmpFolder/regionX-p3.bam 
	//		METRICS_FILE=tmpFolder/regionX-p3-metrics.txt
	//	java MemString -jar toolsFolder/AddOrReplaceReadGroups.jar INPUT=tmpFolder/regionX-p3.bam OUTPUT=tmpFolder/regionX.bam 
	//		RGID=GROUP1 RGLB=LIB1 RGPL=ILLUMINA RGPU=UNIT1 RGSM=SAMPLE1
	// 	java MemString -jar toolsFolder/BuildBamIndex.jar INPUT=tmpFolder/regionX.bam
	//	delete tmpFolder/regionX-p1.bam, tmpFolder/regionX-p2.bam, tmpFolder/regionX-p3.bam and tmpFolder/regionX-p3-metrics.txt
	val cmd1         = javaCommand + "CleanSam.jar" + " INPUT=" + filenameP1 + " OUTPUT=" + filenameP2
	vcBw.write(getTimeStamp + " " + cmd1 + "\n")
	val cmd1res      = (cmd1).!

	val cmd2         = javaCommand + "MarkDuplicates.jar" + " INPUT=" + filenameP2 + " OUTPUT=" + filenameP3 + 
		" METRICS_FILE=" + filenameMetrics
	vcBw.write(getTimeStamp + " " + cmd2 + "\n")
	val cmd2res      = (cmd2).!

	val cmd3         = javaCommand + "AddOrReplaceReadGroups.jar" + " INPUT=" + filenameP3 + " OUTPUT="+ filenameRegBam + 
		" RGID=GROUP1 RGLB=LIB1 RGPL=ILLUMINA RGPU=UNIT1 RGSM=SAMPLE1"
	vcBw.write(getTimeStamp + " " + cmd3 + "\n")
	val cmd3res      = (cmd3).!

	val cmd4         = javaCommand + "BuildBamIndex.jar" + " INPUT=" + filenameRegBam
	vcBw.write(getTimeStamp + " " + cmd4 + "\n")
	val cmd4res      = (cmd4).!

	val cmd5         = "rm " + filenameP1 + " " + filenameP2 + " " + filenameP3 + " " + filenameMetrics
	vcBw.write(getTimeStamp + " " + cmd5 + "\n")
	val cmd5res      = (cmd5).!

	// Make region file 
	//	val tmpBed = new File(tmpFolder/tmpX.bed)
	//	chrRange.writeToBedRegionFile(tmpBed.getAbsolutePath())
	//	toolsFolder/bedtools intersect -a refFolder/ExomeFileName -b tmpFolder/tmpX.bed -header > tmpFolder/bedX.bed
	//	delete tmpFolder/tmpX.bed
	val tmpBed          = new File(filenameTmpBed)
	val tmpBedPath      = tmpBed.getAbsolutePath()
	vcBw.write(getTimeStamp + " " + "Write to Bed Region File.\n")
	chrRange.writeToBedRegionFile(filenameTmpBed)

	val cmd6         = toolsFolder + "bedtools intersect" + " -a " + refFolder + ExomeFileName + " -b " + filenameTmpBed + " -header"
	vcBw.write(getTimeStamp + " " + cmd6 + "\n")
	val cmd6res      = (cmd6 #> new File(filenameBedBed)).!!

	val cmd7         = "rm " + filenameTmpBed
	vcBw.write(getTimeStamp + " " + cmd7 + "\n")
	val cmd7res      = (cmd7).!
	
	// Indel Realignment 
	//	java MemString -jar toolsFolder/GenomeAnalysisTK.jar -T RealignerTargetCreator -nt numOfThreads -R refFolder/RefFileName 
	//		-I tmpFolder/regionX.bam -o tmpFolder/regionX.intervals -L tmpFolder/bedX.bed
	//	java MemString -jar toolsFolder/GenomeAnalysisTK.jar -T IndelRealigner -R refFolder/RefFileName -I tmpFolder/regionX.bam 
	//		-targetIntervals tmpFolder/regionX.intervals -o tmpFolder/regionX-2.bam -L tmpFolder/bedX.bed
	//	delete tmpFolder/regionX.bam, tmpFolder/regionX.bai, tmpFolder/regionX.intervals
	val cmd8         = javaCommand + "GenomeAnalysisTK.jar" + " -T RealignerTargetCreator" + " -nt " + numOfThreads + " -R " + refFolder + RefFileName +
	    " -I " + filenameRegBam + " -o " + filenameRegInt + " -L " + filenameBedBed
	vcBw.write(getTimeStamp + " " + cmd8 + "\n")
	val cmd8res      = (cmd8).!

	val cmd9         = javaCommand + "GenomeAnalysisTK.jar" + " -T IndelRealigner" + " -R " + refFolder + RefFileName + " -I " + filenameRegBam + 
		" -targetIntervals " + filenameRegInt + " -o " + filenameRegBam2 + " -L " + filenameBedBed
	vcBw.write(getTimeStamp + " " + cmd9 + "\n")
	val cmd9res      = (cmd9).!

	val cmd10        = "rm " + filenameRegBam + " " + filenameRegBai + " " + filenameRegInt
	vcBw.write(getTimeStamp + " " + cmd10 + "\n")
	val cmd10res     = (cmd10).!
	
	// Base quality recalibration 
	//	java MemString -jar toolsFolder/GenomeAnalysisTK.jar -T BaseRecalibrator -nct numOfThreads -R refFolder/RefFileName -I 
	//		tmpFolder/regionX-2.bam -o tmpFolder/regionX.table -L tmpFolder/bedX.bed --disable_auto_index_creation_and_locking_when_reading_rods 
	//		-knownSites refFolder/SnpFileName
	//	java MemString -jar toolsFolder/GenomeAnalysisTK.jar -T PrintReads -R refFolder/RefFileName -I 
	//		tmpFolder/regionX-2.bam -o tmpFolder/regionX-3.bam -BQSR tmpFolder/regionX.table -L tmpFolder/bedX.bed 
	// delete tmpFolder/regionX-2.bam, tmpFolder/regionX-2.bai, tmpFolder/regionX.table
	val cmd11         = javaCommand + "GenomeAnalysisTK.jar" + " -T BaseRecalibrator" + " -nct " + numOfThreads + " -R " + refFolder + RefFileName + 
		" -I " + filenameRegBam2 + " -o " + filenameRegTbl + " -L " + filenameBedBed + " --disable_auto_index_creation_and_locking_when_reading_rods" + 
		" -knownSites " + refFolder + SnpFileName
	vcBw.write(getTimeStamp + " " + cmd11 + "\n")
	val cmd11res      = (cmd11).!

	val cmd12         = javaCommand + "GenomeAnalysisTK.jar" + " -T PrintReads" + " -R " + refFolder + RefFileName +
		" -I " + filenameRegBam2 + " -o " + filenameRegBam3 + " -BQSR " + filenameRegTbl + " -L " + filenameBedBed
	vcBw.write(getTimeStamp + " " + cmd12 + "\n")
	val cmd12res      = (cmd12).!

	val cmd13         = "rm " + filenameRegBam2 + " " + filenameRegBai2 + " " + filenameRegTbl
	vcBw.write(getTimeStamp + " " + cmd13 + "\n")
	val cmd13res      = (cmd13).!
	
	// Haplotype -> Uses the region bed file
	// java MemString -jar toolsFolder/GenomeAnalysisTK.jar -T HaplotypeCaller -nct numOfThreads -R refFolder/RefFileName -I 
	//		tmpFolder/regionX-3.bam -o tmpFolder/regionX.vcf  -stand_call_conf 30.0 -stand_emit_conf 30.0 -L tmpFolder/bedX.bed 
	//		--no_cmdline_in_header --disable_auto_index_creation_and_locking_when_reading_rods
	// delete tmpFolder/regionX-3.bam, tmpFolder/regionX-3.bai, tmpFolder/bedX.bed
	val cmd14         = javaCommand + "GenomeAnalysisTK.jar" + " -T HaplotypeCaller" + " -nct " + numOfThreads +" -R " + refFolder + RefFileName + 
		" -I " + filenameRegBam3 + " -o " + filenameVCF + " -stand_call_conf 30.0 -stand_emit_conf 30.0" + " -L " + filenameBedBed + 
		" --no_cmdline_in_header --disable_auto_index_creation_and_locking_when_reading_rods"
	vcBw.write(getTimeStamp + " " + cmd14 + "\n")
	val cmd14res      = (cmd14).!
	vcBw.write(getTimeStamp + " " + "Output written to vcf file" + "\n")

	val cmd15         = "rm " + filenameRegBam3 + " " + filenameRegBai3 + " " + filenameBedBed
	vcBw.write(getTimeStamp + " " + cmd15 + "\n")
	val cmd15res      = (cmd15).!

	vcBw.close
	// return the content of the vcf file produced by the haplotype caller.
	//	Return those in the form of <Chromsome number, <Chromosome Position, line>>
	
	return readVCF(filenameVCF)
}

// read vcf files from tmpFolder
def readVCF(filename: String): Array[(String, (Integer, String))] = {
	//read VCF and map them into <Chromsome number, <Chromosome Position, line>>
	var res = ArrayBuffer.empty[(String, (Integer, String))]

	val br = new BufferedReader(new FileReader(filename))
	var line: String = null
	while ({line = br.readLine; line != null}) {
		if(StringUtils.substring(line, 0, 1) != "#" && line != ""){
			val temp = StringUtils.split(line, "\t")
			val chr  = temp(0)
			val pos  = temp(1).toInt
			val ln   = temp(2) + "\t" + temp(3) + "\t" + temp(4) + "\t" + temp(5) + "\t" + temp(6) + "\t" + 
						temp(7) + "\t" + temp(8) + "\t" + temp(9) 
			res.append((chr, (pos, ln)))
		}
	}
	br.close

	return res.toArray
}

// write into result.vcf
def writeVCF(vcf: Array[(String, (Integer, String))], filename: String) = 
{
	//write combined VCF
	val bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename), "UTF-8"))
	vcf.map{case (a,(b,c)) => (a,b,c)}.foreach(x => bw.write(x._1 + "\t" + x._2 + "\t" + x._3 + "\n"))
	bw.close
}

// load balancing
def balanceLoad(x: (Int, SAMRecord), chromosomesInfo: Map [Int, (Int, Int)], avgn: Int) : 
	Array [(Int, SAMRecord)] =
{
	val limit	= avgn * 1.5
	var key		= x._1
	val sam 	= x._2
	val chrInfo = chromosomesInfo(key)
	var output  = ArrayBuffer.empty[(Int, SAMRecord)]

	if (chrInfo._1 > limit){
		val beginPos = sam.getAlignmentStart()

		if (beginPos > (chrInfo._2 / 2)){
			key = key + NumOfChromosomes
			output.append((key, sam))
		} else {
			output.append((key, sam))
			val endPos = beginPos + sam.getReadLength()
			if (endPos > (chrInfo._2 / 2)){
				output.append((key + NumOfChromosomes, sam))
			}
		}
	} else {
		output.append((key, sam))
	}

	return output.toArray
}

def getListOfFiles(dir: String): List[String] = {
	val d = new File(dir)
	var result = List[String]()
	if (d.exists && d.isDirectory) {
		val files = d.listFiles.filter(_.isFile)
		for (file <- files){
			result ::= file.getName()
		}
	} 
	result
}

def getTimeStamp() : String =
{
	return "[" + new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()) + "] "
}

def main(args: Array[String]) 
{
	val config = new Configuration()
	config.initialize()
		 
	val conf = new SparkConf().setAppName("DNASeqAnalyzer")
	// For local mode, include the following two lines
	conf.setMaster("local[" + config.getNumInstances() + "]")
	conf.set("spark.cores.max", config.getNumInstances())
	
	val sc = new SparkContext(conf)
	
	// Comment these two lines if you want to see more verbose messages from Spark
	Logger.getLogger("org").setLevel(Level.OFF);
	Logger.getLogger("akka").setLevel(Level.OFF);
		
	var t0 = System.currentTimeMillis
	
	// Rest of the code goes here

	// broadcast config
	val configuration       = sc.broadcast(config)

	// get configuration
	val inputFolder         = config.getInputFolder
	val outputFolder        = config.getOutputFolder
	val numOfInstances      = config.getNumInstances.toInt

	// create directory outputFolder/log if it's not exist
	val logFolder			= outputFolder + "log"
	new File(logFolder).mkdirs

	// get list of files from inputFolder (filename of the chuncks)
	val chunksName          = sc.parallelize(getListOfFiles(inputFolder), numOfInstances)

	// BWA Step
	// chrToSamRecord = <chromosome number, SAM record>
	// Example: (11, 11V6WR1:111:D1375ACXX:1:1101:20295:48083 1/2 100b aligned read.)
	val chrToSamRecord      = chunksName.flatMap(x => bwaRun(x, config))
	chrToSamRecord.cache

	// avgSamRecords = average number of SAM records per chromosome
	val avgSamRecords       = (chrToSamRecord.count / NumOfChromosomes).toInt

	// chrToNumSamRecs = the number of SAM records for each chromosome
	// <chromosome number, number of SAM records>
	// Example: (20, 22877)
	val chrToNumSamRecs     = chrToSamRecord.map{case (chr, sam) => (chr, 1)}.reduceByKey(_+_)
	
	// chrInfo = <chromosome number, <number of SAM records, chromosome length>>
	// Example: (20, (22877,63025520))
	val dict                = config.getDict()
	val chrInfo             = chrToNumSamRecs.map{case(chr, nSR) => (chr, (nSR, dict.getSequence(chr).getSequenceLength()))}.collect.toMap
	
	// load balancing
	// chrToSamRecordBal = <chromosome region, SAM record>
	// Example: (11, 11V6WR1:111:D1375ACXX:1:1101:9981:52963 2/2 100b aligned read.)
	val chrToSamRecordBal   = chrToSamRecord.map(x => balanceLoad(x, chrInfo, avgSamRecords)).flatMap(x => x)
	// val numOfRegions        = chrToSamRecordBal.map(x => x._1).reduce((x,y) => math.max(x,y))

	// variant calling
	// vcf = <chromosome, output line>
	// val chrToSamPartitioned = chrToSamRecordBal.partitionBy(new HashPartitioner(numOfRegions + 1))
	val chrToSamPartitioned = chrToSamRecordBal.partitionBy(new HashPartitioner(numOfInstances))
	val vcf = chrToSamPartitioned.mapPartitions(x => variantCallWrapper(x, config))
	
	// write result
	val outputName          = outputFolder + "result.vcf"
	writeVCF(vcf.distinct.sortBy(x => x._1).collect, outputName)

	//to do: log (one folder per part)
	
	val et = (System.currentTimeMillis - t0) / 1000 
	println("|Execution time: %d mins %d secs|".format(et/60, et%60))
}
//////////////////////////////////////////////////////////////////////////////
} // End of Class definition
