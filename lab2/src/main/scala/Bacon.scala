/* Bacon.scala */
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.scheduler._

import java.io._
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.text.DecimalFormat
import java.util.Calendar

import org.apache.hadoop.mapreduce.lib.input.TextInputFormat
import org.apache.hadoop.io.LongWritable
import org.apache.hadoop.io.Text
import org.apache.hadoop.conf.Configuration
import org.apache.commons.lang3.StringUtils


object Bacon 
{
	final val KevinBacon = "Bacon, Kevin (I)"	// This is how Kevin Bacon's name appears in the input file for male actors
	val compressRDDs = false
	// SparkListener must log its output in file sparkLog.txt
	val bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("sparkLog.txt"), "UTF-8"))
	val bw2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("output.txt"), "UTF-8"))
	
	def getTimeStamp() : String =
	{
		return "[" + new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()) + "] "
	}
	
	def main(args: Array[String]) 
	{
		val cores = args(0)				// Number of cores to use
		val inputFileM = args(1)		// Path of input file for male actors
		val inputFileF = args(2)		// Path of input file for female actors (actresses)
		
		val conf = new SparkConf().setAppName("Kevin Bacon app")
		conf.setMaster("local[" + cores + "]")
		conf.set("spark.cores.max", cores)
		//add the option to compress RDD (by changing the value of compressRDDs to either true or false)
		conf.set("spark.rdd.compress", compressRDDs.toString)

		val sc = new SparkContext(conf)
		
		// Add SparkListener's code here
		// SparkListener to print memory consumption of cached RDDs
		sc.addSparkListener(new SparkListener() {
			override def onStageCompleted(stageCompleted: SparkListenerStageCompleted) {
				val rddInfos = stageCompleted.stageInfo.rddInfos
				rddInfos.foreach{x => 
					if(x.isCached){
						bw.write(getTimeStamp() + "" + x.name + ": memsize = "+ x.memSize +
							", diskSize = " + x.diskSize + ", numPartitions = " + x.numPartitions + "\n")
					} else {
						bw.write(getTimeStamp() + "" + x.name + " processed!" + "\n")
					}
				}
			}

			/*
			override def onStageSubmitted(stageSubmitted: SparkListenerStageSubmitted) {
				val rddInfos = stageSubmitted.stageInfo.rddInfos
				rddInfos.foreach{x => 
					if(x.isCached){
						bw.write(getTimeStamp() + "" + x.name + ": memsize = "+ x.memSize +
							", diskSize = " + x.diskSize + ", numPartitions = " + x.numPartitions + "\n")
					} else {
						bw.write(getTimeStamp() + "" + x.name + " processed!" + "\n")
					}
				}
			}
			*/
		});
		
		
		
		println("Number of cores: " + args(0))
		println("Input files: " + inputFileM + " and " + inputFileF)
		
		// Comment these two lines if you want to see more verbose messages from Spark
		Logger.getLogger("org").setLevel(Level.ERROR);
		Logger.getLogger("akka").setLevel(Level.ERROR);
		
		var t0 = System.currentTimeMillis
		
		// Add your main code here
		val config 	= new Configuration()
		config.set("textinputformat.record.delimiter", "\n\n")
		val pathM 	= "/data/sbd2016/lab2input/mod_actors.list"

		val inputActors		= sc.newAPIHadoopFile(inputFileM, classOf[TextInputFormat], classOf[LongWritable], classOf[Text], config)
		val inputActresses	= sc.newAPIHadoopFile(inputFileF, classOf[TextInputFormat], classOf[LongWritable], classOf[Text], config)
		
		//for mod_actors.list 
		//map    : (paragraph to String)
		//map    : foreach paragraph: (line(0), list<lines>)
		//flatMapValues : (line(0), line(i))
		//map    : (actor, movie)
		//filter : filter out tv series and movies that but doesn't contain character sequence "(201" || movies that doesn't have year or movies from before 2010
		//filter : filter out movies from 2010,2017,2018,2019 || only movies 2011-2016
		//map    : (movie, (actor, M)) for movieActors
		//         (movie, (actor, F)) for movieActresses
		val movieActors 	= inputActors.map(_._2.toString
										).map(x => (x.split("\n")(0), x.split("\n"))
										).flatMapValues(x => x
										).map(x => (x._1.split("\t")(0), x._2.split("\t").last)
										).filter(x => (StringUtils.substring(x._2, 0, 1) != "\"" && StringUtils.contains(x._2, "(201") )
										).filter(x => (StringUtils.substring(x._2, StringUtils.indexOf(x._2, "(201")+1, StringUtils.indexOf(x._2, "(201")+5) != "2010" &&
													   StringUtils.substring(x._2, StringUtils.indexOf(x._2, "(201")+1, StringUtils.indexOf(x._2, "(201")+5) != "2017" &&
													   StringUtils.substring(x._2, StringUtils.indexOf(x._2, "(201")+1, StringUtils.indexOf(x._2, "(201")+5) != "2018" &&
													   StringUtils.substring(x._2, StringUtils.indexOf(x._2, "(201")+1, StringUtils.indexOf(x._2, "(201")+5) != "2019")
										).map(x => ((StringUtils.join(StringUtils.substring(x._2, 0, StringUtils.indexOf(x._2, "(201")+5),")")), (x._1, "M"))
										)
		movieActors.setName("rdd_movie2ActorMGender")

		//do the same for mod_actresses.list
		val movieActresses 	= inputActresses.map(_._2.toString
										).map(x => (x.split("\n")(0), x.split("\n"))
										).flatMapValues(x => x
										).map(x => (x._1.split("\t")(0), x._2.split("\t").last)
										).filter(x => (StringUtils.substring(x._2, 0, 1) != "\"" && StringUtils.contains(x._2, "(201") )
										).filter(x => (StringUtils.substring(x._2, StringUtils.indexOf(x._2, "(201")+1, StringUtils.indexOf(x._2, "(201")+5) != "2010" &&
													   StringUtils.substring(x._2, StringUtils.indexOf(x._2, "(201")+1, StringUtils.indexOf(x._2, "(201")+5) != "2017" &&
													   StringUtils.substring(x._2, StringUtils.indexOf(x._2, "(201")+1, StringUtils.indexOf(x._2, "(201")+5) != "2018" &&
													   StringUtils.substring(x._2, StringUtils.indexOf(x._2, "(201")+1, StringUtils.indexOf(x._2, "(201")+5) != "2019")
										).map(x => ((StringUtils.join(StringUtils.substring(x._2, 0, StringUtils.indexOf(x._2, "(201")+5),")")), (x._1, "F"))
										)
		movieActresses.setName("rdd_movie2ActorFGender")

		
		//union movieActors and movieActresses
		//(movie, (actor, gender))
		val movieActorGender= movieActors.union(movieActresses).cache
		movieActorGender.setName("rdd_movie2ActorGender")

		//(actor, gender)
		val actorGender		= movieActorGender.map{case (a, (b,c)) => (b,c)}.reduceByKey((x,y) => x)
		actorGender.setName("rdd_actor2Gender")
		
		//(movie, actor)
		val movieActor		= movieActorGender.map{case (a, (b,c)) => (a,b)}.cache
		movieActor.setName("rdd_movie2Actor")

		//join   		: join actors from the same movies => (movie, (actor, actor))
		//map 			: (actor, actor)
		//distinct		: if two actors collaborating in more than one movie, take only one
		//filter 		: filter out (actor1, actor1) ~ same actor as collaborating actor
		val actorActor 		= movieActor.join(movieActor
										).map{case (a,(b,c)) => (b,c)
										}.distinct(
										).filter(x => x._1 != x._2
										)
		actorActor.setName("rdd_actor2Actor")
		

		//map  : (actor, list<actors>) ~ 
		//list of actors is in the form of 
		//(actor1, actor2#actor3#actor4)
		//(actor2, actor1#actor3#actor4)
		//(actor3, actor1#actor2#actor4)
		val actorListActors 	= actorActor.reduceByKey((a,b) => (a + "#" + b)).cache
		actorListActors.setName("rdd_actor2ListOfActors")
		
		/** 1ST ITERATION **/
		//(actor, distance)
		val actorDistance0		= actorListActors.map(x => (x._1, (if(StringUtils.equals(x._1, KevinBacon)) 0 else 999))).cache
		actorDistance0.setName("rdd_actor2distance0")
		
		
		//(actor, (distance, list<actors>))
		val actorDListActors1	= actorDistance0.leftOuterJoin(actorListActors)
		actorDListActors1.setName("rdd_actor2DistanceListOfActors1")
		
		//(actor, distance1)
		val actorD1 			= actorDListActors1.filter{case (a, (b,c)) => b == 0
												}.map{case (a, (b,c)) => (c.toString)
												}.flatMap(x => x.split("#")
												).map(x => (x, 1)
												).reduceByKey((x,y) => math.min(x,y))
		actorD1.setName("rdd_actor2D1")
		
		//(actor, distance) with newly computed distance
		val actorDistance1 		= actorDistance0.union(actorD1
												).reduceByKey((x,y) => math.min(x,y)
												).cache
		actorDistance1.setName("rdd_actor2Distance1")



		/** 2ND ITERATION **/
		//(actor, (distance, list<actors>))
		val actorDListActors2 	= actorDistance1.leftOuterJoin(actorListActors)
		actorDListActors2.setName("rdd_actor2DistanceListOfActors2")
		
		//(actor, distance2)
		val actorD2 			= actorDListActors2.filter{case (a, (b,c)) => b == 1
												}.map{case (a, (b,c)) => (c.toString)
												}.flatMap(x => x.split("#")
												).map(x => (x, 2)
												).reduceByKey((x,y) => math.min(x,y))
		actorD2.setName("rdd_actor2D2")
		
		//(actor, distance) with newly computed distance
		val actorDistance2 		= actorDistance1.union(actorD2
												).reduceByKey((x,y) => math.min(x,y)
												).cache
		actorDistance2.setName("rdd_actor2Distance2")



		/** 3RD ITERATION **/
		//(actor, (distance, list<actors>))
		val actorDListActors3 	= actorDistance2.leftOuterJoin(actorListActors)
		actorDListActors3.setName("rdd_actor2DistanceListOfActors3")
		
		//(actor, distance3)
		val actorD3				= actorDListActors3.filter{case (a, (b,c)) => b == 2
												}.map{case (a, (b,c)) => (c.toString)
												}.flatMap(x => x.split("#")
												).map(x => (x, 3)
												).reduceByKey((x,y) => math.min(x,y))
		actorD3.setName("rdd_actor2D3")
		
		//(actor, distance) with newly computed distance
		val actorDistance3 		= actorDistance2.union(actorD3
												).reduceByKey((x,y) => math.min(x,y)
												).cache
		actorDistance3.setName("rdd_actor2Distance3")



		/** 4TH ITERATION **/
		//(actor, (distance, list<actors>))
		val actorDListActors4	= actorDistance3.leftOuterJoin(actorListActors)
		actorDListActors4.setName("rdd_actor2DistanceListOfActors4")
		
		//(actor, distance4)
		val actorD4				= actorDListActors4.filter{case (a, (b,c)) => b == 3
												}.map{case (a, (b,c)) => (c.toString)
												}.flatMap(x => x.split("#")
												).map(x => (x, 4)
												).reduceByKey((x,y) => math.min(x,y))
		actorD4.setName("rdd_actor2D4")
		
		//(actor, distance) with newly computed distance
		val actorDistance4 		= actorDistance3.union(actorD4
												).reduceByKey((x,y) => math.min(x,y)
												).cache
		actorDistance4.setName("rdd_actor2Distance4")



		/** 5TH ITERATION **/
		//(actor, (distance, list<actors>))
		val actorDListActors5	= actorDistance4.leftOuterJoin(actorListActors)
		actorDListActors5.setName("rdd_actor2DistanceListOfActors5")
		
		//(actor, distance5)
		val actorD5				= actorDListActors5.filter{case (a, (b,c)) => b == 4
												}.map{case (a, (b,c)) => (c.toString)
												}.flatMap(x => x.split("#")
												).map(x => (x, 5)
												).reduceByKey((x,y) => math.min(x,y))
		actorD5.setName("rdd_actor2D5")
		
		//(actor, distance) with newly computed distance
		val actorDistance5 		= actorDistance4.union(actorD5
												).reduceByKey((x,y) => math.min(x,y)
												).cache
		actorDistance5.setName("rdd_actor2Distance5")



		/** 6TH ITERATION **/
		//(actor, (distance, list<actors>))
		val actorDListActors6	= actorDistance5.leftOuterJoin(actorListActors)
		actorDListActors6.setName("rdd_actor2DistanceListOfActors6")
		
		//(actor, distance6)
		val actorD6				= actorDListActors6.filter{case (a, (b,c)) => b == 5
												}.map{case (a, (b,c)) => (c.toString)
												}.flatMap(x => x.split("#")
												).map(x => (x, 6)
												).reduceByKey((x,y) => math.min(x,y))
		actorD6.setName("rdd_actor2D6")
		
		//(actor, distance) with newly computed distance
		val actorDistance6 		= actorDistance5.union(actorD6
												).reduceByKey((x,y) => math.min(x,y)
												).cache
		actorDistance6.setName("rdd_actor2Distance6")



		//(actor, (gender, distance))
		val actorGenderDistance	= actorGender.join(actorDistance6).cache.setName("rdd_actor2GenderDistance")

		//decimal format for text formatter
		val df1 = new DecimalFormat("#.#")
		val df7 = new DecimalFormat("#.#######")

		//calculate num of movies
		val numOfMovie		= movieActor.map(x => (x._1, 1)).reduceByKey((x,y) => x).count

		//ALL ACTORS

		//calculate num of all actors, male actors, and female actors and their respective percentage
		val numOfActorM		= actorGenderDistance.filter{case (a, (b,c)) => (b=="M")}.count
		val numOfActorF		= actorGenderDistance.filter{case (a, (b,c)) => (b=="F")}.count
		val numOfActorMF	= numOfActorM + numOfActorF

		val percentageOfAM	= df1.format(numOfActorM.toFloat / numOfActorMF.toFloat * 100)
		val percentageOfAF	= df1.format(numOfActorF.toFloat / numOfActorMF.toFloat * 100)

		
		//DISTANCE 1
		
		//calculate num of male actors and female actors and their respective percentage for distance=1
		val numOfActorM1	= actorGenderDistance.filter{case (a, (b,c)) => (c==1 && b=="M")}.count
		val numOfActorF1	= actorGenderDistance.filter{case (a, (b,c)) => (c==1 && b=="F")}.count

		val percentageOfAM1	= df1.format(numOfActorM1.toFloat / numOfActorMF.toFloat * 100)
		val percentageOfAF1	= df1.format(numOfActorF1.toFloat / numOfActorMF.toFloat * 100)

		//DISTANCE 2
		
		//calculate num of male actors and female actors and their respective percentage for distance=2
		val numOfActorM2	= actorGenderDistance.filter{case (a, (b,c)) => (c==2 && b=="M")}.count
		val numOfActorF2	= actorGenderDistance.filter{case (a, (b,c)) => (c==2 && b=="F")}.count

		val percentageOfAM2	= df1.format(numOfActorM2.toFloat / numOfActorMF.toFloat * 100)
		val percentageOfAF2	= df1.format(numOfActorF2.toFloat / numOfActorMF.toFloat * 100)

		//DISTANCE 3
		
		val numOfActorM3	= actorGenderDistance.filter{case (a, (b,c)) => (c==3 && b=="M")}.count
		val numOfActorF3	= actorGenderDistance.filter{case (a, (b,c)) => (c==3 && b=="F")}.count

		val percentageOfAM3	= df1.format(numOfActorM3.toFloat / numOfActorMF.toFloat * 100)
		val percentageOfAF3	= df1.format(numOfActorF3.toFloat / numOfActorMF.toFloat * 100)

		//DISTANCE 4
		
		val numOfActorM4	= actorGenderDistance.filter{case (a, (b,c)) => (c==4 && b=="M")}.count
		val numOfActorF4	= actorGenderDistance.filter{case (a, (b,c)) => (c==4 && b=="F")}.count

		val percentageOfAM4	= df1.format(numOfActorM4.toFloat / numOfActorMF.toFloat * 100)
		val percentageOfAF4	= df1.format(numOfActorF4.toFloat / numOfActorMF.toFloat * 100)

		//DISTANCE 5
		
		val numOfActorM5	= actorGenderDistance.filter{case (a, (b,c)) => (c==5 && b=="M")}.count
		val numOfActorF5	= actorGenderDistance.filter{case (a, (b,c)) => (c==5 && b=="F")}.count

		val percentageOfAM5	= df1.format(numOfActorM5.toFloat / numOfActorMF.toFloat * 100)
		val percentageOfAF5	= df1.format(numOfActorF5.toFloat / numOfActorMF.toFloat * 100)

		//DISTANCE 6
		
		val actorGenderDistance6M = actorGenderDistance.filter{case (a, (b,c)) => (c==6 && b=="M")}.cache.setName("rdd_actor2GenderDistance6M")
		val actorGenderDistance6F = actorGenderDistance.filter{case (a, (b,c)) => (c==6 && b=="F")}.cache.setName("rdd_actor2GenderDistance6F")

		val numOfActorM6	= actorGenderDistance6M.count
		val numOfActorF6	= actorGenderDistance6F.count

		val percentageOfAM6	= df1.format(numOfActorM6.toFloat / numOfActorMF.toFloat * 100)
		val percentageOfAF6	= df1.format(numOfActorF6.toFloat / numOfActorMF.toFloat * 100)

		//DISTANCE 1-6

		val numOfActorM16	= numOfActorM1 + numOfActorM2 + numOfActorM3 + numOfActorM4 + numOfActorM5 + numOfActorM6
		val numOfActorF16	= numOfActorF1 + numOfActorF2 + numOfActorF3 + numOfActorF4 + numOfActorF5 + numOfActorF6
		val numOfActorMF16	= numOfActorM16 + numOfActorF16

		val percentageOfAM16= df7.format(numOfActorM16.toFloat / numOfActorM.toFloat)
		val percentageOfAF16= df7.format(numOfActorF16.toFloat / numOfActorF.toFloat)
		val percentageOfA16 = df7.format(numOfActorMF16.toFloat / numOfActorMF.toFloat)

		//write output (summary)
		bw2.write("Total number of actors = " + numOfActorMF + ", out of which " + numOfActorM + " (" + 
			percentageOfAM + "%) are males while " + numOfActorF + " (" + percentageOfAF + "%) are females.\n")
		bw2.write("Total number of movies = " + numOfMovie + "\n\n")

		bw2.write("There are " + numOfActorM1 + " (" + percentageOfAM1 + "%) actors and " + numOfActorF1 +
			" (" + percentageOfAF1 + "%) actresses at distance 1\n")
		bw2.write("There are " + numOfActorM2 + " (" + percentageOfAM2 + "%) actors and " + numOfActorF2 +
			" (" + percentageOfAF2 + "%) actresses at distance 2\n")
		bw2.write("There are " + numOfActorM3 + " (" + percentageOfAM3 + "%) actors and " + numOfActorF3 +
			" (" + percentageOfAF3 + "%) actresses at distance 3\n")
		bw2.write("There are " + numOfActorM4 + " (" + percentageOfAM4 + "%) actors and " + numOfActorF4 +
			" (" + percentageOfAF4 + "%) actresses at distance 4\n")
		bw2.write("There are " + numOfActorM5 + " (" + percentageOfAM5 + "%) actors and " + numOfActorF5 +
			" (" + percentageOfAF5 + "%) actresses at distance 5\n")
		bw2.write("There are " + numOfActorM6 + " (" + percentageOfAM6 + "%) actors and " + numOfActorF6 +
			" (" + percentageOfAF6 + "%) actresses at distance 6\n\n")

		bw2.write("Total number of actors from distance 1 to 6 = " + numOfActorMF16 + 
			", ratio = " + percentageOfA16 + "\n")
		bw2.write("Total number of male actors from distance 1 to 6 = " + numOfActorM16 + 
			", ratio = " + percentageOfAM16 + "\n")
		bw2.write("Total number of female actors (actresses) from distance 1 to 6 = " + numOfActorF16 + 
			", ratio = " + percentageOfAF16 + "\n")

		//write output (list of names)
		var i = 0
		bw2.write("\nList of male actors at distance 6:\n")
		actorGenderDistance6M.map{case (a, (b,c)) => (a,b)
			}.sortBy(x => x._1
			).collect.foreach{x =>
				i = i + 1
				bw2.write(i + ". " + x._1 + "\n")
			}

		var j = 0
		bw2.write("\nList of female actors (actresses) at distance 6:\n")
		actorGenderDistance6F.map{case (a, (b,c)) => (a,b)
			}.sortBy(x => x._1
			).collect.foreach{x =>
				j = j + 1
				bw2.write(j + ". " + x._1 + "\n")
			}

		sc.stop()
		bw.close()
		bw2.close()
		
		val et = (System.currentTimeMillis - t0) / 1000 
		println("{Time taken = %d mins %d secs}".format(et/60, et%60))
	} 
}
