# Supercomputing for Big Data (ET4310) 2016-2017

## Assignment 1: Using Spark for In-Memory Computation 

- [Assignment](SBD_2016_Lab1.pdf)
- [Code](lab1)
- [Report](lab1-report.md)

## Assignment 2: Six Degrees of Kevin Bacon

- [Assignment](SBD_2016_Lab2.pdf)
- [Code](lab2)
- [Report](lab2-report.md)

## Assignment 3: DNA Analysis

- [Assignment](SBD_2016_Lab3.pdf)
- [Code](lab3)
- [Report](lab3-report.md)
