# Assignment 2

# Six Degrees of Kevin Bacon

*Supercomputing for Big Data (ET4310) 2016-2017*

## Introduction

This assignment aims to solve six degrees of separation problem. The problem is as follows, given the datasets from IMDB containing list of actors and actresses and the movies they performed in, find the degree of separation of each actor/actress to Kevin Bacon.

To solve this problem, several Spark transformations and actions are applied to the input file. Furthermore, SparkListener is implemented in the code to observe the memory consumption and properties of the RDDs.

This report is organized as follows: the first section is Introduction which mainly discusses about brief explanation of the assignment. The second section is Background which covers literature study and background information about technology and terms that are used in this assignment. The next section is Implementation which explains how each exercise is solved. The Result section describes the result of the assignment. The Conclusion section summarizes the report and what has been learnt from this assignment.

## Background

This assignment utilized a lot of Spark transformations and actions to turn the input data (actors.list and actresses.list) from [Movie Database](ftp://ftp.fu-berlin.de/pub/misc/movies/database/) into the desired output (actors/actresses with distance 1-6 to Kevin Bacon).

Spark transformation is Spark operation that transforms an RDD as an input into another RDD as an output through one or more functions. Some transformations supported by Spark are `map()`, `flatMap()`, `filter()`, `union()`, `join()`, `distinct()`, `reduceByKey()`, etc. Spark transformations are lazy and the input file/input RDD is not loaded directly until the contents of the data need to be observed by Spark actions. 

Spark action is Spark operation that observes the contents of the data of an input file of an RDD. Some actions supported by Spark are `count()`, `reduce()`, `collect()`, `first()`, `take()`.

For example, in these statements

```
val textFile = sc.textFile("pagecount.txt")
val mapped = textFile.map(x => x.split(“ “))
val count = mapped.count
```

In the first and second line, `map()` operation is used to transform the file, but at this point the file is not loaded yet. In the third line, which contains count operation, the file is then loaded, read, mapped, and the number of rows will be counted.

In case there is a fourth line as follows

```
val sample = mapped.take(10).foreach(println)
```

The file will be loaded, read, mapped, and 10 rows of that data will be taken. In this example, from line 1-4, `pagecount.txt` file will be read twice (in `count()` function and in `take()` function) but the data is not stored. 

To avoid reading twice, the RDD need to be cached, so that the data will be stored into memory and can be reused without loading and reading again. Note that `cache()` itself is a lazy operation, so until a Spark action is called, it still does nothing. 

```
val textFile = sc.textFile("pagecount.txt")
val mapped = textFile.map(x => x.split(“ “)).cache
val count = mapped.count
val sample = mapped.take(10).foreach(println)
```

In the updated code above, the RDD mapped is cached but until this point, the code still does nothing. In the third line, the input file will then be loaded and processed. In the fourth line, the input file is not processed again but instead using the cached RDD. 

Cache is appropriate to use when the lineage of the RDD branches out or when an RDD is used multiple times and if there is still a lot of free memory. It is inappropriate to use cache when the free memory is low or when the RDD is only processed once.

SparkListener is the interface for listening to events from the Spark scheduler, for example when application start, application end, environment update, stage completed and so on. Those functions can be useful to track the running of application like which RDD takes the longest time to create, how much memory is used to cache an RDD, and so on.

## Implementation

The code implementation for this assignment is mostly following the steps provided in the assignment. The RDD created in each step is explained in the comments of the code.
From SparkListener’s log, the process that takes too long to finish is creating the `<actor, actor>` RDD. In the step for creating RDD `<actor, list<actor>>`, instead of using list, I use concatenated string, separated by “#” instead.

The code can be accessed in [lab2](lab2) directory.

## Results

The goals for the assignment (i.e. the output) is achieved although it’s different from the sample output and it takes more than 10 minutes to complete. The code is run in Kova machine. The parameter for running the application is 4 core processor and 8 GB memory.

The results of the executions are as follow:

|                                   | Uncompressed RDD                              | Compressed RDD                                |
|-----------------------------------|-----------------------------------------------|-----------------------------------------------|
| Running time                      | 12 mins 58 secs                               | 13 mins 50 secs                               |
| Memory consumption of cached RDDs | rdd_movie2Actor:                              | rdd_movie2Actor:                              |
|                                   | memsize = 497688280,                          | memsize = 497688280,                          |
|                                   | diskSize = 0,                                 | diskSize = 0,                                 |
|                                   | numPartitions = 56                            | numPartitions = 56                            |
|                                   |                                               |                                               |
|                                   | rdd_movie2ActorGender:                        | rdd_movie2ActorGender:                        |
|                                   | memsize = 560080336,                          | memsize = 560080336,                          |
|                                   | diskSize = 0,                                 | diskSize = 0,                                 |
|                                   | numPartitions = 56                            | numPartitions = 56                            |
|                                   |                                               |                                               |
|                                   | rdd_actor2distance0:                          | rdd_actor2distance0:                          |
|                                   | memsize = 151816360,                          | memsize = 151816360,                          |
|                                   | diskSize = 0,                                 | diskSize = 0,                                 |
|                                   | numPartitions = 56                            | numPartitions = 56                            |
|                                   |                                               |                                               |
|                                   | rdd_actor2Distance1:                          | rdd_actor2Distance1:                          |
|                                   | memsize = 151411016,                          | memsize = 151411016,                          |
|                                   | diskSize = 0,                                 | diskSize = 0,                                 |
|                                   | numPartitions = 112                           | numPartitions = 112                           |
|                                   |                                               |                                               |
|                                   | rdd_actor2Distance2:                          | rdd_actor2Distance2:                          |
|                                   | memsize = 148883888,                          | memsize = 148883888,                          |
|                                   | diskSize = 0,                                 | diskSize = 0,                                 |
|                                   | numPartitions = 112                           | numPartitions = 112                           |
|                                   |                                               |                                               |
|                                   | rdd_actor2ListOfActors:                       | rdd_actor2ListOfActors:                       |
|                                   | memsize = 2489233392,                         | memsize = 2489233392,                         |
|                                   | diskSize = 0,                                 | diskSize = 0,                                 |
|                                   | numPartitions = 56                            | numPartitions = 56                            |
|                                   |                                               |                                               |
|                                   | rdd_actor2Distance3:                          | rdd_actor2Distance3:                          |
|                                   | memsize = 162717672,                          | memsize = 162717672,                          |
|                                   | diskSize = 0,                                 | diskSize = 0,                                 |
|                                   | numPartitions = 112                           | numPartitions = 112                           |
|                                   |                                               |                                               |
|                                   |                                               |                                               |
|                                   | rdd_actor2Distance4:                          | rdd_actor2Distance4:                          |
|                                   | memsize = 242835096,                          | memsize = 242835096,                          |
|                                   | diskSize = 0,                                 | diskSize = 0,                                 |
|                                   | numPartitions = 112                           | numPartitions = 112                           |
|                                   |                                               |                                               |
|                                   | rdd_actor2Distance5:                          | rdd_actor2Distance5:                          |
|                                   | memsize = 269560744,                          | memsize = 269560744,                          |
|                                   | diskSize = 0,                                 | diskSize = 0,                                 |
|                                   | numPartitions = 112                           | numPartitions = 112                           |
|                                   |                                               |                                               |
|                                   | rdd_actor2Distance6:                          | rdd_actor2Distance6:                          |
|                                   | memsize = 272695000,                          | memsize = 272695000,                          |
|                                   | diskSize = 0,                                 | diskSize = 0,                                 |
|                                   | numPartitions = 112                           | numPartitions = 112                           |
|                                   |                                               |                                               |
|                                   | rdd_actor2GenderDistance:                     | rdd_actor2GenderDistance:                     |
|                                   | memsize = 225461888,                          | memsize = 225461888,                          |
|                                   | diskSize = 0,                                 | diskSize = 0,                                 |
|                                   | numPartitions = 112                           | numPartitions = 112                           |
|                                   |                                               |                                               |
|                                   | rdd_actor2GenderDistance6M: memsize = 510696, | rdd_actor2GenderDistance6M: memsize = 510696, |
|                                   | diskSize = 0,                                 | diskSize = 0,                                 |
|                                   | numPartitions = 112                           | numPartitions = 112                           |


In the result above, if the RDDs are compressed, the running time is one minute longer. The memory consumption of compressed RDDs should be smaller than the uncompressed RDDs. However, in the code solution that I made, there are no difference in memory consumption of compressed and uncompressed RDDs.

## Conclusion

In this assignment, I learned about the usage of Spark transformations and actions techniques to manipulate input data and `SparkListener` to “listen” to event or schedule during Spark runtime. In this assignment, I also learned when the RDDs should be cached and when it shouldn’t be. Furthermore, I learned about how to use different kind of function in Spark to reduce the running time of the program.




