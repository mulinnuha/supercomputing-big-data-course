import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j.Logger
import org.apache.log4j.Level
import java.io._
import java.util.Locale
import java.lang.Math
import org.apache.commons.lang3.StringUtils


object Pagecounts 
{
	// Gets Language's name from its code
	def getLangName(code: String) : String =
	{
		new Locale(code).getDisplayLanguage(Locale.ENGLISH)
	}

	def main(args: Array[String]) 
	{
		val inputFile = args(0) // Get input file's name from this command line argument
		val conf = new SparkConf().setAppName("Pagecounts")
		val sc = new SparkContext(conf)
		
		// Uncomment these two lines if you want to see a less verbose messages from Spark
		Logger.getLogger("org").setLevel(Level.OFF);
		Logger.getLogger("akka").setLevel(Level.OFF);
		
		val t0 = System.currentTimeMillis
		
		// Add your code here

		//make an rdd from input file
		val pagecounts 	 = sc.textFile(inputFile)

		//for each row, split by space
		//map to (languageCode, pageTitle, viewCount, pageSize)
		val row 		 = pagecounts.map(x => (x.split(" ")))

		//filter out row where languageCode = pageTitle
		val filtered 	 = row.filter(x => StringUtils.substringBefore(x(0),".") != StringUtils.substringBefore(x(1),"."))

		//find totalView and maxView per languangeCode
		//map "filtered" to (languageCode, (viewCount,viewCount))
		//reduceByKey to (languageCode, (totalView, maxView))
		val langTotalMax = filtered.map(x => (StringUtils.substringBefore(x(0),"."), (x(2).toInt, x(2).toInt))).reduceByKey((x,y) => (x._1 + y._1, math.max(x._2 , y._2)))

		//map from (languageCode, (totalView, maxView)) to ((maxView, languageCode), totalView)
	    val maxLangTotal = langTotalMax.map{case (lang, (total, max)) => ((max, lang), total)}

	    //map "filtered" to ((viewCount, languageCode), pageTitle)
	    val maxLangTitle = filtered.map(x => ((x(2).toInt, StringUtils.substringBefore(x(0),".")), x(1)))

	    //join "maxLangTotal" with "maxLangTitle"
	    //the result should be ((maxView, languageCode), (totalView, pageTitle))
	    //if there are more than one row with same key (maxView, languageCode), pick the first by using reduceByKey
	    val maxLangTotalTitle = maxLangTotal.join(maxLangTitle).reduceByKey((x,y) => x)
	    
	    //sort descending by key (maxView, languageCode)
	    //map the result into (languageCode, totalView, pageTitle, maxView)
	    val sorted 		 = maxLangTotalTitle.sortByKey(false).map{case ((max, lang), (total, title)) => (lang, total, title, max)}
		
		//write to file
		val bw 	= new BufferedWriter(new OutputStreamWriter(new FileOutputStream("part1.txt"), "UTF-8"))
		bw.write("Language,Language-code,TotalViewsInThatLang,MostVisitedPageInThatLang,ViewsOfThatPage\n")
	    for (result <- sorted.collect) {
	       bw.write(getLangName(result._1) + "," + result._1 + "," + result._2 + "," + result._3 + "," + result._4 + "\n")
	    }
	    bw.close

		val et = (System.currentTimeMillis - t0) / 1000
		System.err.println("Done!\nTime taken = %d mins %d secs".format(et / 60, et % 60))
	}
}

