import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j.Logger
import org.apache.log4j.Level
import java.io._
import java.util.Locale
import org.apache.commons.lang3.StringUtils

object AnalyzeTwitters
{
	// Gets Language's name from its code
	def getLangName(code: String) : String =
	{
		return new Locale(code).getDisplayLanguage(Locale.ENGLISH)
	}
	
	def main(args: Array[String]) 
	{
		val inputFile = args(0)
		val conf = new SparkConf().setAppName("AnalyzeTwitters")
		val sc = new SparkContext(conf)
		
		// Comment these two lines if you want more verbose messages from Spark
		Logger.getLogger("org").setLevel(Level.OFF);
		Logger.getLogger("akka").setLevel(Level.OFF);
		
		val t0 = System.currentTimeMillis
		
		// Add your code here

		//load file
		val data 	= sc.textFile(inputFile)

		//filter out header
		val header 	= data.first()
		val content = data.filter(row => row != header)

		//split by comma
		val data1 	= content.map(x => x.split(","))

		//find maximum value of MaxRetweetCount (MaxMaxRetweetCount) and minimum value of MinRetweetCount (MinMinRetweetCount)
		//map to ((Language-code,IDOfTweet,Text) , (MaxRetweetCount,MinRetweetCount))
		//reduced to ((Language-code,IDOfTweet,Text) , (MaxMaxRetweetCount,MinMinRetweetCount))
		val data2	= data1.map(x => ((x(2),x(4),x(8)), (x(5).toInt,x(6).toInt))).reduceByKey((x,y) => (math.max(x._1,y._1), math.min(x._2, y._2)))

		//calculate RetweetCount
		//map data2 to ((Language-code) , (IDOfTweet,Text,RetweetCount))
		val data3	= data2.map{case ((y1,y2,y3), (y4,y5)) => ((y1), (y2,y3,y4-y5+1))}

		//calculate TotalRetweetsInThatLang
		//map data3 to (Language-code, RetweetCount)
		//reduce to (LanguageCode, TotalRetweetsInThatLang)
		val data4	= data3.map{case ((y1), (y2,y3,y4)) => ((y1,y4))}.reduceByKey(_ + _)

		//join data3 and data4
		//the result should be ((Language-code), (IDOfTweet,Text,RetweetCount,TotalRetweetsInThatLang))
		val data5	= data3.leftOuterJoin(data4)

		//map to (Language-code,IDOfTweet,Text,RetweetCount,TotalRetweetsInThatLang)
		//filter out tweets with retweet < 2
		//sort (descending) by TotalRetweetInThatLang and RetweetCount
		val sorted 	= data5.map{case ((y1), ((y2,y3,y4), y5)) => (y1,y2,y3,y4,y5)}.filter(_._4 >= 2).sortBy(x => (x._5,x._4), false)

		val bw 	= new BufferedWriter(new OutputStreamWriter(new FileOutputStream("part3.txt"), "UTF-8"))
		bw.write("Language,Language-code,TotalRetweetsInThatLang,IDOfTweet,RetweetCount,Text\n")
	    for (result <- sorted.collect) {
	       bw.write (getLangName(result._1) + "," + result._1 + "," + result._5 + "," + result._2 + "," + result._4 + "," + result._3 + "\n")
	    }
	    bw.close

		// outRDD would have elements of type String.
		// val outRDD = ...
		// Write output
		/*val bw = new BufferedWriter(new OutputStreamWriter(System.out, "UTF-8"))
		bw.write("Language,Language-code,TotalRetweetsInThatLang,IDOfTweet,RetweetCount,Text\n")
		outRDD.collect.foreach(x => bw.write(x))
		bw.close*/
		
		val et = (System.currentTimeMillis - t0) / 1000
		System.err.println("Done!\nTime taken = %d mins %d secs".format(et / 60, et % 60))
	}
}

