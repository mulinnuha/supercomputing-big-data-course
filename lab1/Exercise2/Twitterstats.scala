import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.spark.streaming._
import org.apache.spark.streaming.twitter._
import org.apache.spark.streaming.StreamingContext._
import java.io._
import java.util.Locale
import org.apache.tika.language.LanguageIdentifier
import java.util.regex.Matcher
import java.util.regex.Pattern

object Twitterstats
{ 
	var firstTime = true
	var t0: Long = 0
	val bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("twitterLog.txt"), "UTF-8"))
	
	// This function will be called periodically after each 5 seconds to log the output. 
	// Elements of a are of type (lang, totalRetweetsInThatLang, idOfOriginalTweet, text, maxRetweetCount, minRetweetCount)
	def write2Log(a: Array[(String, Long, Long, String, Long, Long)])
	{
		if (firstTime)
		{
			bw.write("Seconds,Language,Language-code,TotalRetweetsInThatLang,IDOfTweet,MaxRetweetCount,MinRetweetCount,RetweetCount,Text\n")
			t0 = System.currentTimeMillis
			firstTime = false
		}
		else
		{
			val seconds = (System.currentTimeMillis - t0) / 1000
			
			if (seconds < 60)
			{
				println("Elapsed time = " + seconds + " seconds. Logging will be started after 60 seconds.")
				return
			}
			
			println("Logging the output to the log file\nElapsed time = " + seconds + " seconds\n-----------------------------------")
			
			for(i <-0 until a.size)
			{
				val langCode = a(i)._1
				val lang = getLangName(langCode)
				val totalRetweetsInThatLang = a(i)._2
				val id = a(i)._3
				val textStr = a(i)._4.replaceAll("\\r|\\n", " ")
				val maxRetweetCount = a(i)._5
				val minRetweetCount = a(i)._6
				val retweetCount = maxRetweetCount - minRetweetCount + 1
				
				bw.write("(" + seconds + ")," + lang + "," + langCode + "," + totalRetweetsInThatLang + "," + id + "," + 
					maxRetweetCount + "," + minRetweetCount + "," + retweetCount + "," + textStr + "\n")
			}
		}
	}
  
	// Pass the text of the retweet to this function to get the Language (in two letter code form) of that text.
	def getLang(s: String) : String =
	{
		val inputStr = s.replaceFirst("RT", "").replaceAll("@\\p{L}+", "").replaceAll("https?://\\S+\\s?", "")
		var langCode = new LanguageIdentifier(inputStr).getLanguage
		
		// Detect if japanese
		var pat = Pattern.compile("\\p{InHiragana}") 
		var m = pat.matcher(inputStr)
		if (langCode == "lt" && m.find)
			langCode = "ja"
		// Detect if korean
		pat = Pattern.compile("\\p{IsHangul}");
		m = pat.matcher(inputStr)
		if (langCode == "lt" && m.find)
			langCode = "ko"
		
		return langCode
	}
  
	// Gets Language's name from its code
	def getLangName(code: String) : String =
	{
		return new Locale(code).getDisplayLanguage(Locale.ENGLISH)
	}
  
	def main(args: Array[String]) 
	{
		// Configure Twitter credentials
		val apiKey = "WEXnkv6XlwBAyt2Jub3KIZ0hb"
		val apiSecret = "HrfKGuMiV5ipGvGTC5i7cag6wkpFMP7gcsE4TPvaHOwRa6U5ME"
		val accessToken = "39760857-IwNFDkzZKuiOK5rxTw04OCUEBNQOvkAbEH32OZpfy"
		val accessTokenSecret = "tABtCyVzmcKwFat8xUZFRMV7otKJMra6pj5VLKBkwhlUL"
		
		Helper.configureTwitterCredentials(apiKey, apiSecret, accessToken, accessTokenSecret)
		
		val ssc = new StreamingContext(new SparkConf(), Seconds(5))
		val tweets = TwitterUtils.createStream(ssc, None)
		
		// Add your code here
		//filter only tweet which is a retweet
		val retweets 	= tweets.filter(x => x.isRetweet())

		//map to (idOfOriginalTweet, text, lang, retweetCount, retweetCount)
		val statuses 	= retweets.map(x => (x.getRetweetedStatus().getId(), x.getText(), getLang(x.getText()), x.getRetweetedStatus().getRetweetCount().toLong, x.getRetweetedStatus.getRetweetCount().toLong))
		
		//for each RDD in window(60,5) do the following
		val stream 		= statuses.window(Seconds(60), Seconds(5)).foreachRDD(rdd => {
			//map rdd to ((idOfOriginalTweet, text, lang), (retweetCount, retweetCount))
			//reduce to ((idOfOriginalTweet, text, lang), (maxRetweetCount, minRetweetCount))
			//map to ((lang), (idOfOriginalTweet, text, maxRetweetCount, minRetweetCount))
			val data1 	= rdd.map{case (x1,x2,x3,x4,x5) => ((x1,x2,x3), (x4,x5))}.reduceByKey((x,y) => (math.max(x._1,y._1), math.min(x._2,y._2))).map{case ((x1,x2,x3), (x4,x5)) => ((x3), (x1,x2,x4,x5))}
			
			//map rdd to (lang, retweetCount)
			//reduce to (lang, totalRetweetsInThatLang)
			val data2 	= rdd.map{case (x1,x2,x3,x4,x5) => (x3,x4)}.reduceByKey((x,y) => (x+y))

			//join data1 and data2
			//the result should be ((lang), ((idOfOriginalTweet, text, maxRetweetCount, minRetweetCount) , (totalRetweetsinThatLang))
			val data3 	= data1.leftOuterJoin(data2)

			//map to (lang, totalRetweetsInThatLang, idOfOriginalTweet, text, maxRetweetCount, minRetweetCount)
			//sort (descending) by 
			val sorted	= data3.map{case ((x3), ((x1,x2,x4,x5), (x6))) => (x3,x6.get,x1,x2,x4,x5)}.sortBy(y => (y._2, y._5-y._6+1), false)

			//write the result to log file
			write2Log(sorted.collect)
		})

		// If elements of RDDs of outDStream aren't of type (lang, totalRetweetsInThatLang, idOfOriginalTweet, text, maxRetweetCount, minRetweetCount),
		//	then you must change the write2Log function accordingly.
		//results.foreachRDD(rdd => write2Log(rdd.collect))
	
		new java.io.File("cpdir").mkdirs
		ssc.checkpoint("cpdir")
		ssc.start()
		ssc.awaitTermination()
	}
}

