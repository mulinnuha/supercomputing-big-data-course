# Assignment 1

# Using Spark for In-Memory Computation

*Supercomputing for Big Data (ET4310) 2016-2017*

## Introduction

This assignment consists of 3 (three) exercises about Using Spark for In-Memory Computation. The first exercise is about using _Spark_ to calculate total and maximum page view per language from Wikipedia pagecounts data. The second exercise is about using _Spark Streaming_ to retrieve tweets from twitter and perform analysis on them. The result of the second exercise is then used for input for further analysis in the third exercise.

Several popular functions in Spark like `map`, `reduceByKey`, `filter`, and `sortBy` are used to solve the exercises. After the data source is loaded, these functions are used to map and calculate the desired output which is then written to file. 

This report is organized as follows: the first section is Introduction which mainly discusses about brief explanation of the assignment. The second section is Background which covers literature study and background information about technology and terms that are used in this assignment. The next section is Implementation which explains how each exercise is solved. The Result section describes the result of the assignment. The Conclusion section summarizes the report and what has been learnt from this assignment. And lastly, the References mention the web pages that are used as references for this assignment.

## Background

This assignment is solved using Apache Spark, an open source data processing framework, and Scala programming language with some help from Java libraries.

There are several terms related to Apache Spark:
- Spark or Apache Spark is an open source framework for data processing in distributed clusters. Spark is written in Scala, Java, Python, and R.
- Scala is an object-oriented programming language that runs on Java virtual machine. It can import Java libraries and use them in the code. Scala is used to solve this assignment.
- RDD or Resilient Distributed Dataset is a concept in Spark which is a fault-tolerant collection of elements. This collection can be operated in parallel and can be created by parallelizing an existing collection in driver program (using `parallelize`) or referencing dataset from external storage (for example, using `textFile`). In exercise 1 and 3, RDDs are created from external file.
- DStream or Discretized Stream is a continuous sequence of RDDs representing a continuous stream of data. An example of DStream is Twitter streaming that was used in exercise 2. While a Spark Streaming program is running, each DStream periodically generates a RDD. In exercise 2, DStreams from twitter are manipulated in a window of 60 seconds with slide interval of 5 seconds.
- Spark can be used to do batch processing and stream processing. The characteristics of batch processing system are it has access to all data, it might compute something big and complex, and it is generally more concerned with throughput than latency of individual components. Meanwhile stream processing system has the following characteristics: it computes a function of one data element or a smallish window of recent data, it needs to complete each computation in near-real-time, and asynchronous which means source of data doesn’t interact with the stream processing directly, like by waiting for an answer. In this assignment, exercise 1 and 3 used batch processing while exercise 2 used stream processing.
- In-memory processing is a technology to process data which is stored in an in-memory database i.e. database system that relies on main memory for computer data storage.
- The advantages of Spark over Hadoop MapReduce are mainly in terms of speeds of processing. Spark runs in-memory on the cluster and it isn’t tied to Hadoop’s MapReduce two-stage paradigm. This makes faster processing and repeated access to the same data. Spark is also written in Scala, Java, Python, and R so it is relatively easier to program.

## Implementation

Most of information on what the code did are already written in the comment of the code. The following information describe how the code turn input data into the desired output. 

The code can be accessed in [lab1](lab1) directory.

### Exercise 1:

**Input**

[Pagecounts data from Wikipedia](https://dumps.wikimedia.org/other/pagecounts-raw/2016/2016-08/)

```
<Language code> <Page title> <View count> <Page size>
```

**Output**

[Sample](https://www.dropbox.com/s/zp6npdxqaviu51w/part1.txt?dl=0)

```
<Language>, <Language-code>, <TotalViewsInThatLang>, <MostVisitedPageInThatLang>, <ViewsOfThatPage>
```

**Process**

1. Make an RDD from inputFile
2. Map RDD from step 1 into (`languageCode`, `pageTitle`, `viewCount`, `pageSize`)	
3. Filter out row where the `languageCode` is the same as `pageTitle`
4. Map RDD into ((`languageCode`), (`viewCount`, `viewCount`)) and then perform `reduceByKey` function to calculate `totalView` in each `languageCode` and `maxView` in each `languageCode`
5. Map the result from step 4 into ((`maxView`, `languageCode`), (`totalView`))
6. Make another RDD from RDD in step 3 with elements (`viewCount`, `pageTitle`)
7. Join the RDD from step 5 and 6. The result is RDD with elements ((`maxView`, `languageCode`), (`totalView`, `pageTitle`))
8. Sort descending the data by maxView and languageCode and map them into (`languageCode`, `totalView`, `pageTitle`, `maxView`)

### Exercise 2

**Input**

Twitter Streams

**Output**

([Sample](https://www.dropbox.com/s/9vbrytyofalc94g/part2.txt?dl=0))

```
<Secs>, <Lang>, <Langcode>, <TotalRetweetsInThatLang>, <IDOfTweet>, <MaxRetweetCount>, <MinRetweetCount>, <RetweetCount>, <Text>
```

**Process**

1.	Filter tweets from twitter stream, only get tweet which is a retweet
2.	Map RDD from step 1 into (idOfOriginalTweet, text, languageCode, retweetCount, retweetCount)
3.	In every window length of 60 seconds and slide interval of 5 seconds, do the following

    a. Map RDD into ((`idOfOriginalTweet`, `text`, `languageCode`), (`retweetCount`, `retweetCount`)) 
    and reduce to ((`idOfOriginalTweet`, `text`, `languageCode`), (`maxRetweetCount`, `minRetweetCount`)). Then map to ((`languageCode`), (`idOfOriginalTweet`, `text`, `maxRetweetCount`, `minRetweetCount`))

    b. Map RDD into (`languageCode`, `retweetCount`) and reduce to (`languageCode`, `totalRetweetsInThatLang`)

    c. Join RDD from step (a) and (b). The result is RDD with elements ((`languageCode`), ((`idOfOriginalTweet`, `text`, `maxRetweetCount`, `minRetweetCount`),(`totalRetweetsInThatLang`)))

    d. Map the result from step c to (`languageCode`, `totalRetweetsInThatLang`, `idOfOriginalTweet`, `text`, `maxRetweetCount`, `minRetweetCount`)

    e. Sort (descending) by totalRetweetsInThatLang, retweetCount
    
    f. Collect, write to log file


### Exercise 3

**Input**

```
Seconds, Language, Language-code, TotalRetweetsInThatLang, IDOfTweet, MaxRetweetCount, MinRetweetCount, RetweetCount, Text
```

**Output**

[Sample](https://www.dropbox.com/s/bnsgpm9m03anz6f/part3.txt?dl=0)

```
Language, Languagecode, TotalRetweetsInThatLang, IDOfTweet, RetweetCount, Text
```

**Process**

1. Make an RDD from inputFile
2. Filter out header row
3. Make an RDD with elements (`Seconds`, `Language`, `Language-code`, `TotalRetweetsInThatLang`, `IDOfTweet`, `MaxRetweetCount`, `MinRetweetCount`, `RetweetCount`, `Text`)

4. Map the RDD from step 3 to ((`Language-code`, `IDOfTweet`, `Text`), (`MaxRetweetCount`, `MinRetweetCount`) and reduce to ((`Language-code`, `IDOfTweet`, `Text`), (`MaxOfMaxRetweetCount`, `MinOfMinRetweetCount`)
5. Map the RDD from step 4 to ((`Languagecode`), (`IDOfTweet`, `Text`, `RetweetCount`))
6. Map the RDD from step 5 to (`Languagecode`, `RetweetCount`) and reduce to (`Languagecode`, `TotalRetweetsInThatLang`)
7. Join RDD from step 5 with RDD from step 6. The result is an RDD with elements ((`Languagecode`), ((`IDOfTweet`, `Text`, `RetweetCount`), (`TotalRetweetsInThatLang`)))
8. Map RDD from step 7 to (`Languagecode`, `IDOfTweet`, `Text`, `RetweetCount`, `TotalRetweetsInThatLang`)
9. Filter out `retweetCount` < 2
10. Sort descending by `TotalRetweetsInThatLang`, `RetweetCount`

## Results

| Exercise | Input name                 | Input size | Output name    | Output size | Execution time                                                     |
|----------|----------------------------|------------|----------------|-------------|--------------------------------------------------------------------|
| 1        | pagecounts-20160801-000000 | 340.3 MB   | part1.txt      | 13 KB       | 39 seconds                                                         |
| 2        | Twitter Stream             | -          | twitterLog.txt | 590 KB      | 71 seconds (log is written 3 times in 61st, 66th, and 71th second) |
| 3        | part2.txt (sample file)    | 22.2 MB    | part3.txt      | 114 KB      | 4 seconds                                                          |

## Conclusion

In this assignment, I have learnt how to do data computation using Spark and Spark Streaming, what is RDD and DStreams, how to manipulate them. In the first and third exercise of the assignment, I learn how to use map function to transform data into certain format and configuration, `reduceByKey` function to combine data using associative `reduce` function, `sortBy` and `sortByKey` to sort data, `filter` function to filter out unwanted data, and `join` function to join two RDDs. In the second exercise I learn how to use Spark streaming to get twitter stream and how to manipulate them, window function to perform analysis in a specific slide interval in a window of time.

The techniques used in this assignment can be explored more to do analysis for example sentiment analysis on twitter, log analysis on web server, etc.

## References

- [http://softwareengineeringdaily.com/2015/08/03/batch-vs-streaming-the-differences/](http://softwareengineeringdaily.com/2015/08/03/batch-vs-streaming-the-differences/)
- [https://en.wikipedia.org/wiki/Apache_Spark](https://en.wikipedia.org/wiki/Apache_Spark) 
- [https://en.wikipedia.org/wiki/In-memory_processing](https://en.wikipedia.org/wiki/In-memory_processing)
- [https://en.wikipedia.org/wiki/Scala_(programming_language)](https://en.wikipedia.org/wiki/Scala_(programming_language))
- [https://spark.apache.org/docs/1.2.0/programming-guide.html](https://spark.apache.org/docs/1.2.0/programming-guide.html )
- [https://www.xplenty.com/blog/2014/11/apache-spark-vs-hadoop-mapreduce/](https://www.xplenty.com/blog/2014/11/apache-spark-vs-hadoop-mapreduce/) 
