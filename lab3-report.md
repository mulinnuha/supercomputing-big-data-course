# Assignment 3

# DNA Analysis

*Supercomputing for Big Data (ET4310) 2016-2017*

## Introduction

This assignment is mainly about using the distribution capabilities of Spark. The problem that’s needed to be solved is post-sequencing DNA analysis using GATK (Genome Analysis Toolkit) in distributed computing infrastructure. DNA analysis has become the bottleneck in using the large data sets, as it requires powerful and scalable tools to perform the needed analysis. By implementing an in-memory distributed version of GATK DNA Analysis, hopefully the execution time would be reduced.

There has been a previous study of post-sequencing DNA Analysis in Cluster-Based infrastructure [1]. It utilized Apache Spark and Spark’s capability for distributed computing to solve the problem. I mainly followed the procedure mentioned in the study to finish this lab exercises.

This report is organized as follows. This introduction section is followed by Background which covers the Spark and Scala concepts used in this assignment. The next section, Implementation, describes the implementation of the code to solve the problem. Results section presents the result of the code implementation and Conclusion discussed the summary of the report.

## Background
This is an important part of this report. Here you must demonstrate your understanding of Spark and Scala concepts used in this assignment, such as broadcast variables, Scala’s process package, mapPartitions and foreachPartitions, Hadoop and Yarn etc. Also give some background of the problem, which is Post sequence DNA Analysis and how it can be solved using big data techniques.
Several concepts in Spark and Scala that were used in this assignment. The following is the list of them.

1. Scala’s process package
Scala’s process package is the package in Scala to execute system commands. Process that’s usually done from command line or terminal can be executed within the program. This is useful when, for example, we need to call another tools in our program that can only be run via terminal. In this assignment, Scala’s process package is used to execute BWA tools and GATK tools.

2. Broadcast variables
Broadcast variables are variables that are shared and can be accessed throughout the cluster. However, these variables should fit in the memory (not very large variables), and are immutable (cannot be changed later). In this assignment, the config variable should be the broadcast variable that can be accessed throughout the cluster.

3. `foreachPartition`
`foreachPartition` is a function in Spark RDD that can be used to execute a function for each partition in that RDD. Iterator argument provides the access to the content in the partition.

4. `mapPartitions`
`mapPartitions` is a function in Spark RDD that is similar to map function but is called once in each partition in that RDD. The content of each partition can be accessed via `Iterator`. `mapPartitions` function returns another `Iterator` and the combined iterator for all partition are saved into a new RDD. In this assignment, `mapPartitions` is used in Variant Calling step in exercise 2 where data in each partition is sent to the variant call function.

5. `mapPartitionsWithIndex`
Similar to mapPartitions with the addition of the index of partition as the parameter for the custom function. The first assignment utilizes mapPartitionsWithIndex to write chunk files and use the index as the filename.

6. `Custom Record Reader`
Default Record Reader can read input file as treat one line as one record. Since one DNA short reads consists of 4 lines, custom record reader is needed to process the Fastq files.

## Implementation

The implementation for the first exercise is mainly using `zipPartitions` to make DNA short reads interleave, `partitionBy` to partition the RDD, and `mapPartitionsWithIndex` to write the chunk files per partition. The partitioner that was used in this exercise is `HashPartition`. Since the Fastq files are read per four lines, a custom RecordReader is needed. For this, I used the code from Data Algorithms book with slight modification [2].

For the second exercise, the implementation is mainly following the procedures in paper [1]. The chunk files from the first exercise are sent as parameter for several DNA Analysis Toolkit using Scala’s process package. The distributed part is that the process is done in parallel by applying partitioning. There is slight different in the code from the one in paper [1]. In the paper, the number of partitions is the same as the number of chromosome regions while in this code, the number of partitions is managed via parameters (`numOfInstances`) with the maximum value of 4.

The code can be accessed in [lab3](lab3) directory.

## Results

The results for the first and the second exercise are achieved. The system that was used to run the code for both exercise is Kova Machine.

The results of the first exercise are n chunk files with interleaved DNA short reads. The size of each file is slightly different to each other due to partitioning process. The summary of the input, output, and the results is as follows

| Input         | Output           | Execution Time |
|---------------|------------------|----------------|
| + inputFolder | + outputFolder   | 15 secs        |
|   - fastq1.fq |   - chunk1.fq.gz |                |
|   - fastq2.fq |   - chunk2.fq.gz |                |
|               |   - ...          |                |
|               |   - chunkn.fq.gz |                |

The results of the second exercise are a vcf file and several log files in output folder. The vcf file is then compared to reference file to check the accuracy and correctness of the file. This is done by using comparison.py script. The comparison result, along with the input and output description is as follows

| Input          | Output           | Execution Time | Comparison result |
|----------------|------------------|----------------|-------------------|
| + inputFolder  | + outputFolder   | 12 mins 1 secs | total = 4031      |
| - chunk1.fq.gz | - result.vcf     |                | matches = 4011    |
| - chunk2.fq.gz | + log            |                | diff = 20         |
| - ...          | + bwa            |                |                   |
| - chunkn.fq.gz | - chunk1_log.txt |                |                   |
|                | - chunk2_log.txt |                |                   |
|                | - ...            |                |                   |
|                | - chunkn_log.txt |                |                   |
|                | + vc             |                |                   |
|                | - x_log.txt      |                |                   |
|                | - y_log.txt      |                |                   |
|                | - ...            |                |                   |
|                | - z_log.txt      |                |                   |

## Conclusion

In this assignment, I learned a lot about the usage of Spark and Scala for parallel processing, especially the usage of mapPartitions and mapPartitionsWIthIndex method. I also learned about Scala’s process package that can be used to call external command from within the Spark application. Moreover, I learned about post-sequence DNA analysis in general and how to optimize it by using Apache Spark.

## References

[1] Mushtaq, H., Al-Ars, Z.: Cluster-based Apache Spark implementation of the GATK DNA analysis pipeline. In: IEEE International Conference on Bioinformatics and Biomedicine, pp. 1471–1477. IEEE Computer Society (2015)

[2] Parsian, M. 2015. Data Algorithms: Recipes for Scaling Up with Hadoop and Spark. O'Reilly Media.
